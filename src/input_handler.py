import socket
from src.exceptions import WhoisNotFound
import src.reply_text as reply_text

from enum import Enum
from src.tracer import run


class State(Enum):
    SET_MAX_TTL = 1
    DEFAULT = 0


class ArgsHandler:

    state = State.DEFAULT
    max_ttl = 20

    def set_max_ttl(self, word: str):

        number = int(word)

        if word == "0":
            print(reply_text.ZERO_TTL)
            exit(0)

        if number > 255 or number < 1:
            print(reply_text.TTL_OUT_OF_RANGE)
            exit(0)

        self.max_ttl = number
        self.state = State.DEFAULT

    def handle_word(self, word: str):
        if word == "-m":
            self.state = State.SET_MAX_TTL
            return

        if word in ("-h", "--help"):
            print(reply_text.HELP)
            return

        # TODO: обработать ошибки строки ниже

        addr = socket.gethostbyname(word)
        try:
            run(addr, self.max_ttl)
        except PermissionError:
            print(reply_text.PERMISSION_DENIED)
        except socket.gaierror:
            print(reply_text.NO_INTERNET_CONNECTION)
        except WhoisNotFound:
            print(reply_text.WHOIS_NOT_FOUND)
        exit(0)

    def __init__(self) -> None:
        pass

    def read_word(self, args):
        for word in args:
            self.transition[self.state](self, word)

    transition = {State.DEFAULT: handle_word, State.SET_MAX_TTL: set_max_ttl}
