class NoInternetConnection(BaseException):
    pass

class WhoisNotFound(BaseException):
    pass


class IpNotFoundError(BaseException):
    def __init__(self, ip: str) -> None:
        super().__init__()
        self.ip = ip
