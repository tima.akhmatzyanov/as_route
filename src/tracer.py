from src.icmpBuilder import IcmpBuilder
from src.icmp_utils import ping
from src.exceptions import IpNotFoundError
from src.ip_info import IpInfo
from src.row_formatter import row_format


def run(address, max_ttl):

    row_format.add(["", "AS", "ip", "cc", "desc"])

    for i in range(1, max_ttl + 1):

        resp = ping(address, timeout=0.5, ttl=i)
        if resp is None:
            row_format.add([i, "***", "", "", ""]).print()
            continue

        addr, icmp = resp

        try:
            ip_info = IpInfo.from_address(addr)
        except IpNotFoundError as e:
            row_format.add([i, "", e.ip, "", ""]).print()

        row_format.add([i, *ip_info.to_list()]).print()

        if icmp.type == IcmpBuilder.ICMP_ECHO_REPLY:
            break
