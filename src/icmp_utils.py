import random
import select
import socket
import struct
from src.icmpBuilder import IcmpBuilder


def create_packet(id):
    return IcmpBuilder().set_type(8).set_code(0).set_id(id).pack()


def receive_ping(my_socket, timeout):
    # Receive the ping from the socket.
    while True:
        ready = select.select([my_socket], [], [], timeout)
        if ready[0] == []:  # Timeout
            return
        rec_packet, addr = my_socket.recvfrom(1024)
        icmp_packet = IcmpBuilder.unpack(rec_packet)
        return (addr[0], icmp_packet)


def ping(dest_addr, ttl=1, timeout=1):

    with socket.socket(
        socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP
    ) as my_socket:
        _ttl = struct.pack("b", ttl)
        my_socket.setsockopt(socket.IPPROTO_IP, socket.IP_TTL, _ttl)
        packet_id = int((id(timeout) * random.random()) % 65535)
        packet = create_packet(packet_id)
        while packet:
            sent = my_socket.sendto(packet, (dest_addr, 1))
            packet = packet[sent:]
        resp = receive_ping(my_socket, timeout)
    return resp
