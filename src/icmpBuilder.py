import socket
import struct

from src.rfc1071 import ichecksum


class IcmpBuilder:

    checksum: int
    type: int
    code: int
    id: int
    DATA = b"Q" * 64
    ICMP_ECHO_REPLY = 0

    def __init__(self):
        self.checksum = 0
        self.code = 0
        self.type = 0
        self.id = 0

    def set_checksum(self, checksum: int):
        self.checksum = checksum
        return self

    def set_type(self, type: int):
        self.type = type
        return self

    def set_code(self, code: int):
        self.code = code
        return self

    def set_id(self, id: int):
        self.id = id
        return self

    @staticmethod
    def unpack(ip_packet: bytes):
        icmp_header = ip_packet[20:28]
        type, code, checksum, p_id, sequence = struct.unpack("bbHHh", icmp_header)
        return (
            IcmpBuilder()
            .set_type(type)
            .set_code(code)
            .set_id(p_id)
            .set_checksum(checksum)
        )

    def pack(self) -> bytes:
        # Header is type (8), code (8), checksum (16), id (16), sequence (16)
        header = struct.pack("bbHHh", self.type, self.code, 0, self.id, 1)
        my_checksum = ichecksum(header + IcmpBuilder.DATA)
        header = struct.pack(
            "bbHHh", self.type, self.code, socket.htons(my_checksum), self.id, 1
        )
        return header + IcmpBuilder.DATA
