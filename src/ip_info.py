import subprocess
import re

from src.exceptions import IpNotFoundError, NoInternetConnection, WhoisNotFound


class IpInfo:
    ip: str
    as_number: str
    country: str
    providers: list[str]

    def __init__(
        self, ip: str, as_number: str, country: str, providers: list[str]
    ) -> None:
        self.ip = ip
        self.as_number = as_number
        self.country = country
        self.providers = providers

    def to_list(self) -> list:
        return [self.as_number, self.ip, self.country, self.providers]

    @staticmethod
    def is_whois_installed():
        try:
            subprocess.run(["whois", "--help"], capture_output=True)
        except FileNotFoundError:
            return False
        return True

    @staticmethod
    def from_address(address: str):

        if not IpInfo.is_whois_installed():
            raise WhoisNotFound

        cymru = subprocess.run(
            ["whois", "-h", "whois.cymru.com", "--", f"-v {address}"],
            capture_output=True,
            encoding="utf-8",
        )
        if len(cymru.stdout.split("\n")) <= 1:
            raise NoInternetConnection
        # AS | IP | BGP Prefix | CC | Registry | Allocated | AS Name
        ip_info = list(map(lambda s: s.strip(), cymru.stdout.split("\n")[1].split("|")))
        if len(ip_info) != 7:
            raise IpNotFoundError(address)

        radb = subprocess.run(
            ["whois", "-h", "whois.radb.net", ip_info[1]],
            capture_output=True,
            encoding="utf-8",
        )
        providers = list(set(re.findall(r"descr:\s*(.+)", radb.stdout)))
        return IpInfo(ip_info[1], ip_info[0], ip_info[3], providers)
