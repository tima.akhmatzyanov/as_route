from collections import deque


class RowFormatter:

    row_format: list[int]

    output: deque[str]

    def __init__(self, row_format: list[int]) -> None:
        self.row_format = row_format
        self.output = deque()

    def create_row(self, data: list) -> str:
        if len(data) != len(self.row_format):
            raise ValueError

        out = ""
        for n, s in zip(self.row_format, data):
            out += fill_spaces(str(s), n)

        return out

    def add(self, data: list):
        self.output.append(self.create_row(data))
        return self

    def print(self) -> list[str]:
        for el in self.output:
            print(el)
        self.output.clear()


def fill_spaces(s: str, length: int) -> str:
    diff = length - len(s)
    if diff < 0:
        diff = 0
    return s + " " * diff


row_format = RowFormatter([3, 7, 16, 3, 1])
