ZERO_TTL = "first hop out of range"
TTL_OUT_OF_RANGE = "0 < max hops < 256"
PERMISSION_DENIED = "can not create socket, permission denied"
NO_INTERNET_CONNECTION = "no internet connection"
# нужно ли взять из input handler'а?
HELP = """ AStrace.py [flags] [IPv4 / Domain name]
    flags:
        -m [0 < int < 256 ] - set max TTL to be reached (default 20)  
"""
WHOIS_NOT_FOUND = "whois not found"