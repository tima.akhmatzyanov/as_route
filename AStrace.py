import sys
from src.input_handler import ArgsHandler

if __name__ == "__main__":
    ArgsHandler().read_word(sys.argv[1:])
